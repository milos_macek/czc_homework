import { StatusBar } from 'expo-status-bar';
import { StyleSheet, View } from 'react-native';
import { enableScreens } from 'react-native-screens';
import Navigation from './app/navigation/navigation';

enableScreens();

export default function App() {
  return (
    <View style={styles.container}>
      <StatusBar />
      <Navigation />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  }
});