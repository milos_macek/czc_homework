import React, { useState } from 'react';
import { StyleSheet, Text, View, Dimensions, TouchableOpacity, Image } from 'react-native';
import { FlatGrid } from 'react-native-super-grid';
import cuid from 'cuid';
import { ButtonComponent } from './button.component';
import { usePersistStore, useStore } from '../store/store';
import { useNavigation } from '@react-navigation/native';

export const DetailComponent = (props: any) => {
    const navigation: any = useNavigation();
    const { item } = props;
    const width = Math.floor(Dimensions.get('window').width) - 40;
    const [cardWidth, setCardWidth] = useState<number>(Math.floor(width / 3 - 20));

    const onLayout = (e: any) => {
        const dim = Math.floor(width / 3 - 20);
        setCardWidth(dim);
    };

    const renderImage = ({ item, index }: any) => {
        const height = Math.floor((cardWidth / 4) * 3);

        return (
            <TouchableOpacity
                key={cuid()}
                onPress={() => openGallery(index)}
                activeOpacity={0.7}>
                <View
                    style={[styles.itemContainer, { width: cardWidth }]}>
                    <Image source={{ uri: item }} style={[styles.photo, { width: "100%", height: height }]} />
                </View>
            </TouchableOpacity>
        );
    }

    const handleCloseDetail = () => {
        useStore.getState().setDetailPlace(null);
    }

    const handleSelectPlace = () => {
        usePersistStore.getState().selectPlace(item.id);
        useStore.getState().setDetailPlace(null);
    }

    const openGallery = async (index: number) => {
        navigation.navigate("GalleryScreen", { photos: item.photos, index: index })
    }

    return item ? (
        <View style={styles.container}>
            <View style={styles.innerContainer}>
                <View style={styles.header}>
                    <Text style={styles.title}>{item.title}</Text>
                </View>
                <Text style={styles.text}>{item.address}</Text>
                <Text>
                    <Text style={styles.shipingFree}>Doprava zdarma </Text>
                    <Text style={styles.text}>{item.availability}</Text>
                </Text>
                <View style={styles.openings}>
                    {item.openingHours.openingHours.map((day: any) => (
                        <View style={styles.openingRow}>
                            <Text style={styles.day}>{day.dayName}</Text>
                            <Text style={styles.opening}>{day.morningOpeningHours || "Zavřeno"}</Text>
                        </View>
                    ))}
                </View>
                {(item.photos.length > 0) &&
                    <View onLayout={onLayout} style={styles.gridContainer}>
                        <FlatGrid
                            scrollIndicatorInsets={{ right: 1 }}
                            itemDimension={cardWidth}
                            data={item.photos}
                            style={styles.gridView}
                            renderItem={renderImage}
                        />
                    </View>}
                <ButtonComponent title={"Skrýt detail pobočky"} outline onButtonPress={handleCloseDetail} />
                <ButtonComponent title={"Vyzvednout zde"} onButtonPress={handleSelectPlace} />
            </View>
        </View>
    ) : null;
}

const styles = StyleSheet.create({
    container: {
        width: "100%"
    },
    innerContainer: {
        borderWidth: 0.5,
        borderColor: "#E6E5E5",
        margin: 10,
        borderRadius: 5,
        padding: 10
    },
    header: {
        height: 24
    },
    title: {
        fontSize: 14,
        fontWeight: "bold",
    },
    text: {
        fontSize: 14,
        fontWeight: "normal",
        paddingVertical: 5
    },
    shipingFree: {
        fontSize: 14,
        fontWeight: "bold",
        color: "green"
    },
    openings: {
        marginVertical: 10
    },
    openingRow: {
        paddingVertical: 2,
        flexDirection: "row"
    },
    day: {
        flex: 1
    },
    opening: {
        flex: 1
    },
    gridContainer: {
        width: "100%",
        paddingHorizontal: 0,
        paddingBottom: 0
    },
    gridView: {},
    itemContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    photo: {
        resizeMode: "cover",
    },
})