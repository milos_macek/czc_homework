import React, { useState, useEffect } from 'react';
import MapView, { Marker } from 'react-native-maps';
import * as _ from 'lodash';
import getCenter from "geolib/es/getCenter";
import Supercluster from 'supercluster';
import { getLongitudeDeltaFromZoom, getMapBounds } from "../util/helper";
import { SafeAreaView } from 'react-native-safe-area-context';
import { StyleSheet, Dimensions, View } from 'react-native';
import { AppConfig } from '../config/app.config';
import { useStore } from '../store/store';
import MapClusterMarker from './mapClusterMarker.component';
import { SvgMarkers } from './svgMarkers.component';

const width = Math.floor(Dimensions.get('window').width);
const height = Math.floor(Dimensions.get('window').height);

export function MapComponent({ children, onSelect, items }: any) {
    const [mapRef, setMapRef] = useState(null);
    const [mapReady, setMapReady] = useState(false);
    const [mapBounds, setMapBounds] = useState(null);
    const [clustererIndex, setClustererIndex] = useState(null);
    const [clusterLoaded, setClusterLoaded] = useState(false);
    const { setDetailPlace } = useStore((state: any) => state);
    const [region, setRegion] = useState({
        latitude: AppConfig.map.mapCenter.latitude,
        longitude: AppConfig.map.mapCenter.longitude,
        latitudeDelta: AppConfig.map.mapZoom
            ? getLongitudeDeltaFromZoom(AppConfig.map.mapZoom)
            : AppConfig.map.mapDelta,
        longitudeDelta: AppConfig.map.mapZoom
            ? getLongitudeDeltaFromZoom(AppConfig.map.mapZoom)
            : AppConfig.map.mapDelta * (width / height)
    });

    useEffect(() => {
        let markers: any[] = [];

        if (!items) {
            return;
        }

        for (let item of items) {
            markers.push({
                ...item,
                geometry: {
                    coordinates: [item.coordinates.longitude, item.coordinates.latitude],
                    type: "Point"
                }
            });
        }

        let cluster: any = new Supercluster({
            //radius: 100,
            minPoints: 4,
            extent: 512,
            maxZoom: 16
        })

        cluster.load(markers);
        setClustererIndex(cluster);
        setClusterLoaded(true);

        const centerAll: any = getCenter(markers);
        moveMapToLocation(centerAll.latitude, centerAll.longitude, 0.2);

    }, [items]);

    const handleMapReady = () => {
        setMapReady(true);

    };

    const handleRegionChangeComplete = (region: any) => {
        if (!mapReady) {
            return;
        }

        setRegion(region);
        setMapBounds(getMapBounds(region));
    };

    const handleMarkerPress = (item: any) => {
        if (item.isCluster) {
            zoomToCluster(item);
        } else {
            setDetailPlace(item);
        }
    }

    const zoomToCluster = (place: any) => {
        let zoom;

        if (clustererIndex) {
            zoom = clustererIndex.getClusterExpansionZoom(place.properties.cluster_id);
        }

        if (zoom) {
            let newLongitudeDelta = getLongitudeDeltaFromZoom(zoom);
            let region = {
                latitude: place.geometry.coordinates[1],
                longitude: place.geometry.coordinates[0],
                latitudeDelta: 0.00001,
                longitudeDelta: newLongitudeDelta
            };

            if (mapRef) {
                mapRef.animateToRegion(region);
            }
        }
    }

    const moveMapToLocation = (lat: number, lng: number, delta?: number) => {
        if (mapRef && lat && lng) {
            mapRef.animateToRegion(
                {
                    latitude: lat,
                    longitude: lng,
                    latitudeDelta: delta || AppConfig.map.mapDelta,
                    longitudeDelta: (delta || AppConfig.map.mapDelta) * (width / height)
                }
            );
        }
    }

    const getObjectsAndClusters = () => {
        if (clustererIndex && mapBounds) {
            let placesAndClusters = clustererIndex.getClusters(
                [mapBounds.westLng, mapBounds.southLat, mapBounds.eastLng, mapBounds.northLat],
                mapBounds.zoom
            );

            if (placesAndClusters) {
                for (let place of placesAndClusters) {
                    place.isCluster = !!(place.properties && place.properties.cluster);
                    place.properties = {
                        ...place.properties,
                        zoom: mapBounds.zoom
                    };
                }
            }
            return placesAndClusters;
        } else {
            return [];
        }
    }

    const getCenterOffsetForAnchor = (anchor: any, markerWidth: number, markerHeight: number) => {
        return {
            x: (markerWidth * 0.5) - (markerWidth * anchor.x),
            y: (markerHeight * 0.5) - (markerHeight * anchor.y),
        };
    }

    const renderObject = (data: any) => {
        if (data.properties.cluster) {
            return (<MapClusterMarker
                key={data.properties.cluster_id}
                count={data.properties.point_count}
            />)
        } else {
            return (<View style={{
                width: 37,
                height: 50,
                backgroundColor: 'rgba(52, 52, 52, 0)'
            }}>
                {data.type === "CZC" ? SvgMarkers.czc : SvgMarkers.ulozenka}
            </View>)
        }
    }

    return (
        <SafeAreaView style={[styles.container]}>
            <MapView
                style={styles.mapStyle}
                ref={(map: any) => setMapRef(map)}
                initialRegion={region}
                mapType={AppConfig.map.initMapType}
                onRegionChangeComplete={handleRegionChangeComplete}
                onMapReady={handleMapReady}
                customMapStyle={customMapsMapStyle}
                showsUserLocation={true}
                zoomControlEnabled={true}
                rotateEnabled={false}
                moveOnMarkerPress={false}
                showsMyLocationButton={false}
                toolbarEnabled={false}
                pitchEnabled={true}
                showsTraffic={false}
                showsBuildings={false}
                showsIndoors={false}
                followsUserLocation={false}
                showsCompass={false}>
                {mapReady && clusterLoaded &&
                    getObjectsAndClusters().map((place: any, i: number) => (
                        <Marker
                            key={place.properties.cluster_id ? place.properties.cluster_id : `${place.id}${i}`}
                            title={place.title}
                            tracksViewChanges={true}
                            tracksInfoWindowChanges={true}
                            anchor={{ x: 0.5, y: 1 }}
                            centerOffset={getCenterOffsetForAnchor({ x: 0.5, y: 1 }, 37, 50)}
                            onPress={() => handleMarkerPress(place)}
                            coordinate={{
                                latitude: place.geometry.coordinates[1],
                                longitude: place.geometry.coordinates[0]
                            }}>
                            {renderObject(place)}
                        </Marker>
                    ))}
            </MapView>
        </SafeAreaView>
    );
};

const customMapsMapStyle = [
    {
        featureType: 'poi',
        stylers: [
            {
                visibility: 'off'
            }
        ]
    }
];

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    mapStyle: {
        ...StyleSheet.absoluteFillObject
    },
    layersButtonContainer: {
        position: 'absolute',
        right: 20,
        top: 20
    }
});