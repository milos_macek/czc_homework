import React from 'react';
import { StyleSheet, View, TextInput, TouchableOpacity } from "react-native";
import { Ionicons as Icon } from '@expo/vector-icons';

export const SearchBox = (props: any) => {
    const { onChangeText, onLocationPress, value } = props;

    return (
        <View
            style={styles.container} >
            <View style={styles.leftIcon}>
                <Icon
                    size={25}
                    name="search"
                    color={"#333"}
                />
            </View>
            <View style={styles.formContainer}>
                <TextInput
                    value={value}
                    onChangeText={onChangeText}
                    placeholder={"Zadejte adresu..."}
                    style={styles.form}
                    keyboardType="default"
                />
            </View>
            <TouchableOpacity style={styles.rightIcon} onPress={onLocationPress}>
                <Icon
                    size={25}
                    name="locate"
                    color={"#487998"}
                />
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        position: "absolute",
        top: 10,
        left: 10,
        right: 10,
        height: 45,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        borderRadius: 5,
        borderWidth: 1,
        borderColor: "#ccc",
        backgroundColor: "#fff",
        padding: 10
    },
    leftIcon: {
        height: 25,
        width: 25,
    },
    rightIcon: {
        height: 25,
        width: 25,

    },
    formContainer: {
        flex: 1,
        width: "100%"
    },
    form: {
        paddingHorizontal: 10,
        width: "100%",
        height: 25,
    }
});