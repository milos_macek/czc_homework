
import React from 'react';
import { StyleSheet, TouchableOpacity, Text, View } from 'react-native';

export const ListItem = (props: any) => {
    const { title, address, icon, selected, onItemPress } = props;

    return (
        <TouchableOpacity
            style={[styles.container, selected && styles.selected]}
            onPress={onItemPress}>
            <View style={styles.topRowContainer}>
                <Text style={styles.title}>{title}</Text>
            </View>
            <View style={styles.bottomRowContainer}>
                <Text style={styles.subtitle}>{address}</Text>
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        borderWidth: 0.5,
        borderColor: "#E6E5E5",
        marginHorizontal: 10,
        marginVertical: 5,
        borderRadius: 5,
        padding: 10
    },
    selected: {
        borderWidth: 1,
        borderColor: "#CD001F",
    },
    topRowContainer: {
        flexDirection: "row",
        justifyContent: "flex-start",
        marginBottom: 5
    },
    bottomRowContainer: {
        flexDirection: "row",
        justifyContent: "flex-start",
        marginTop: 5
    },
    title: {
        fontSize: 14,
        fontWeight: "bold"
    },
    subtitle: {
        fontSize: 14,
        fontWeight: "normal"
    },

});
