import React from 'react';
import { StyleSheet, Text, View, FlatList, ActivityIndicator } from 'react-native';
import { usePersistStore, useStore } from '../store/store';
import { ListItem } from './listItem.component';

export const ListComponent = ({ hide, items, onLoadMore, searching }: any) => {
    const { loading, moreLoading, isListEnd, setDetailPlace } = useStore((state: any) => state);
    const { selectedPlaceId } = usePersistStore((state: any) => state);

    const loadMore = () => {
        if (!isListEnd && !moreLoading && !searching) {
            onLoadMore();
        }
    }

    const renderFooter = () => (
        <View style={styles.footerText}>
            {moreLoading && <ActivityIndicator />}
            {isListEnd && !searching && <Text>Nejsou žádná další místa...</Text>}
        </View>
    )

    const renderEmpty = () => (
        <View style={styles.emptyText}>
            <Text>Žádná místa...</Text>
        </View>
    )

    const renderItem = ({ item }: any) => (
        <ListItem
            title={item.title}
            address={item.address}
            icon={item.type === "CZC" ? "ico-circle-czc" : "ico-circle-ulozenka"}
            selected={item.id === selectedPlaceId}
            onItemPress={() => handleItemPress(item)} />
    );

    const handleItemPress = (item: any) => {
        setDetailPlace(item);
    }

    return (
        <View style={[styles.container, hide && { height: 0 }]}>
            {loading ?
                <View style={styles.loading}>
                    <ActivityIndicator size='large' />
                </View>
                :
                <View>
                    <View style={styles.headerContainer}>
                        <Text style={styles.title}>Seznam míst</Text>
                    </View>
                    <FlatList
                        contentContainerStyle={{ flexGrow: 1 }}
                        data={items}
                        keyExtractor={(item: any) => item.id}
                        renderItem={renderItem}
                        ListFooterComponent={renderFooter}
                        ListEmptyComponent={renderEmpty}
                        onEndReachedThreshold={0.2}
                        onEndReached={loadMore}
                    />
                </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: "100%"
    },
    headerContainer: {
        paddingTop: 20,
        paddingBottom: 10,
        paddingHorizontal: 20,
        alignSelf: "flex-start"
    },
    title: {
        fontSize: 14
    },
    loading: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    footerText: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 10
    },
    emptyText: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})