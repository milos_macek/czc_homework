import React from 'react';
import { StyleSheet, TouchableOpacity, Text } from "react-native";

export const ButtonComponent = (props: any) => {
    const { title, onButtonPress, outline } = props;

    return (
        <TouchableOpacity
            style={[styles.container, outline && styles.containerOutline]}
            onPress={onButtonPress} >
            <Text style={[styles.text, outline && styles.textOutline]}>{title}</Text>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        flexDirection: "row",
        justifyContent: "center",
        paddingVertical: 10,
        backgroundColor: "#CD001F",
        borderRadius: 5,
        borderWidth: 2,
        borderColor: "#CD001F",
        marginBottom: 10
    },
    containerOutline: {
        backgroundColor: "#fff",
        borderColor: "#487998"
    },
    button: {
        backgroundColor: "#CD001F",
        padding: 5
    },
    text: {
        fontSize: 14,
        fontWeight: "bold",
        color: "#fff"
    },
    textOutline: {
        color: "#487998"
    }
});