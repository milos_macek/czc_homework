import React from 'react';
import cuid from 'cuid';
import { StyleSheet, Image, Dimensions } from 'react-native';
import PagerView from 'react-native-pager-view';

const width = Math.floor(Dimensions.get('window').width);
const height = Math.floor((width / 4) * 3);

export default function GalleryScreen({ route, navigation }: any) {
    const { photos, index } = route.params;

    return (
        <PagerView style={styles.pagerView}
            initialPage={index}>
            {photos.map((photo: any) => (
                <Image key={cuid()} source={{ uri: photo }} style={styles.photo} />
            ))}
        </PagerView>
    );
};

const styles = StyleSheet.create({
    pagerView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#000",
        paddingBottom: 50
    },

    photo: {
        width: width,
        height: height,
        resizeMode: "contain",
    }
});