import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    View,
} from 'react-native';
import * as _ from 'lodash';
import { DetailComponent } from '../components/detail.component';
import { ListComponent } from '../components/list.component';
import { useStore } from '../store/store';
import { SearchBox } from '../components/searchBox.component';
import { removeDiacritics } from '../util/helper';
import { MapComponent } from '../components/map.component';

export default function PlacesScreen({ navigation }: any) {
    const [page, setPage] = useState(1)
    const [typing, setTyping] = useState("");
    const [searchResults, setSearchResults] = useState([]);
    const [searching, setSearching] = useState(false);
    const items = useStore((state: any) => state.data?.items);
    const detailPlace = useStore((state: any) => state.detailPlace);
    const { fetchMorePlaces } = useStore((state: any) => state);

    useEffect(() => {
        fetchMorePlaces(page);
    }, [page]);

    useEffect(() => {

        if (typing.length > 3) {
            let foundItems: any = _.filter(items, (item: any) => {
                if ((removeDiacritics(item.title).indexOf(typing) != -1)
                    || (removeDiacritics(item.address).indexOf(typing) != -1)) {
                    return item;
                }
            })
            setSearchResults(foundItems);
        } else if (typing.length > 0 && typing.length <= 3) {
            setSearching(true);
            setSearchResults([]);
        } else {
            setSearching(false);
            setSearchResults([]);
        }
    }, [typing]);

    const handleLoadMore = () => {
        setPage(page + 1);
    }

    return (
        <View style={styles.container}>
            <View style={styles.mapContainer}>
                <MapComponent
                    items={searching ? searchResults : items} />
                <SearchBox
                    value={typing}
                    onChangeText={(text: string) => setTyping(removeDiacritics(text).replace(/[0-9]/g, ""))} />
            </View>
            <View style={[styles.placesContainer, detailPlace && { flex: "auto" }]}>
                <DetailComponent
                    item={detailPlace} />
                <ListComponent
                    hide={detailPlace}
                    items={searching ? searchResults : items}
                    onLoadMore={handleLoadMore}
                    searching={searching} />
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%",
        backgroundColor: '#fff',
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    mapContainer: {
        flex: 2,
        width: "100%",
        backgroundColor: "#ccc"
    },
    placesContainer: {
        flex: 3,
        width: "100%",
    }

});