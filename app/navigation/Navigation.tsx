import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { RootStackParamList } from '../types/types';
import PlacesScreen from '../screens/places.screen';
import GalleryScreen from '../screens/gallery.screen';

export default function Navigation() {

  return (
    <NavigationContainer>
      <RootNavigator />
    </NavigationContainer>
  );
}

const Stack = createStackNavigator<RootStackParamList>();

function RootNavigator() {
  return (
    <Stack.Navigator screenOptions={{
      headerShown: true
    }}>
      <Stack.Screen
        name="PlacesScreen"
        component={PlacesScreen}
        options={{
          headerTitle: "Osobní odběr",
        }}
      />
      <Stack.Screen
        name="GalleryScreen"
        component={GalleryScreen}
        options={{
          headerTitle: "Galerie",
          headerBackTitle: "",
          headerTruncatedBackTitle: "",
          headerTintColor: '#fff',
          headerStyle: {
            backgroundColor: "#000"
          },
        }}
      />
    </Stack.Navigator>
  );
}
