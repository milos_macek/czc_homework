export const removeDiacritics = (str: string) => {
    return str.toLowerCase().replace(/á/g, "a").replace(/é/g, "e").replace(/í/g, "i").replace(/ó/g, "o").replace(/[úů]/g, "u").replace(/ý/g, "y").replace(/[ĺľ]/g, "l")
        .replace(/č/g, "c").replace(/ď/g, "d").replace(/ě/g, "e").replace(/ň/g, "n").replace(/ř/g, "r").replace(/š/g, "s").replace(/ť/g, "t").replace(/ž/g, "z");
}

export const getLongitudeDeltaFromZoom = (zoom: number) => {
    return Math.pow(Math.E, Math.log(360) - (zoom + 1) * Math.LN2);
}

export const getMapBounds = (region: any) => {
    return {
        westLng: region.longitude - region.longitudeDelta / 2,
        southLat: region.latitude - region.latitudeDelta / 2,
        eastLng: region.longitude + region.longitudeDelta / 2,
        northLat: region.latitude + region.latitudeDelta / 2,
        zoom: Math.floor(Math.log(360 / region.longitudeDelta) / Math.LN2)
    };
}