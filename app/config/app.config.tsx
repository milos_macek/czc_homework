export const AppConfig: any = {
    apiUrl: "https://pickups-example.sarman.workers.dev/?page=",
    map: {
        mapCenter: {
            latitude: 51.757614,
            longitude: 15.050825
        },
        mapZoom: 5,
        mapDelta: 0.005,
        initMapType: "standard"
    },
}