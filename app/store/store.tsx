import create from "zustand";
import { persist, devtools } from "zustand/middleware";
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { AppConfig } from "../config/app.config";

export const useStore = create(
    devtools((set: any) => ({
        loading: false,
        moreLoading: false,
        error: "",
        data: null,
        isListEnd: false,
        detailPlace: null,
        fetchMorePlaces: async (page: number) => {
            let data: any = null, error: string = "", isListEnd: boolean = false;

            set((state: any) => {
                data = state.data;
                return page === 1 ? { loading: true } : { moreLoading: true }
            });

            try {
                const response = await axios(AppConfig.apiUrl + page);

                if (response.status === 200 && response.data) {
                    if (data) {
                        data = {
                            ...response.data,
                            items: data.items.concat(response.data.items)
                        };
                        isListEnd = page === data.pager.pagesTotal
                    } else {
                        data = response.data;
                    }
                } else {
                    error = "Server error";
                }
            } catch (error) {
                error = error;
            }

            set((state: any) => ({
                loading: false,
                moreLoading: false,
                data: data,
                error: error,
                isListEnd: isListEnd
            }));

        },
        setDetailPlace: (place: any) => set((state: any) => ({ detailPlace: place }))
    })))

export const usePersistStore = create(
    persist(
        (set, get) => ({
            selectedPlaceId: null,
            selectPlace: (placeId: string) => set({ selectedPlaceId: placeId }),
        }),
        {
            name: 'place-storage', // unique name
            getStorage: () => AsyncStorage
        }
    )
);